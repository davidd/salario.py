nombre_1 = "Ana"
nombre_2 = "Pepe"


def impuestos(nomina_1, nomina_2):
    impuestos_1 = nomina_1 * 0.30
    impuestos_2 = nomina_2 * 0.30
    impuesto = impuestos_1 + impuestos_2
    return impuesto

def coste_salarial(nomina_1, nomina_2):
    impuestos_1 = nomina_1 * 0.30
    impuestos_2 = nomina_2 * 0.30
    coste_salarial = (impuestos_1 + nomina_1) + (impuestos_2 + nomina_2)
    return coste_salarial


print("Los impuestos a pagar son", impuestos(30000, 20000))  
print("La nómina a pagar es", coste_salarial(30000, 20000)) 

